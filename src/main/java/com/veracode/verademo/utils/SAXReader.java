package com.veracode.verademo.utils;
import java.net.URL;
import org.dom4j.*;
//import org.dom4j.io.SAXReader;

public class SAXReader {
    public Document parse(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.parse(url); 
        return document;
}
}